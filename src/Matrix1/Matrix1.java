package Matrix1;

public class Matrix1 {
    public static void main(String[] args) {
        int a = 4;
        int b = 4;
        int[][] matrixA = new int[a][b];
        matrixA[0][0] = 1;
        matrixA[0][1] = -2;
        matrixA[0][2] = 3;
        matrixA[0][3] = 4;
        matrixA[1][0] = 1;
        matrixA[1][1] = 7;
        matrixA[1][2] = 6;
        matrixA[1][3] = 2;
        matrixA[2][0] = 5;
        matrixA[2][1] = 2;
        matrixA[2][2] = 9;
        matrixA[2][3] = 4;
        matrixA[3][0] = 1;
        matrixA[3][1] = 6;
        matrixA[3][2] = 7;
        matrixA[3][3] = 1;

        int s = 0;
        for (int i = a/2; i < a; i++) {
            for (int j = 0; j < b/2; j++) {
                s += matrixA[i][j];
            }
        }

        for (int i = 0; i < a/2; i++) {
            for (int j = b/2; j < b; j++) {
                s += matrixA[i][j];
            }
        }
        System.out.println(s);
    }
}
